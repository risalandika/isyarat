﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Isyarat
{
    class ListDataSet
    {
        private List<DataSet> dataSetList;
        public List<DataSet> DataSetList
        {
            get
            {
                return dataSetList;
            }
        }
        public int Count
        {
            get
            {
                return dataSetList.Count;
            }
        }
        public DataSet this[int index]
        {
            set
            {
                dataSetList[index] = value;
            }
            get
            {
                return dataSetList[index];
            }
        }

        public ListDataSet()
        {
            dataSetList = new List<DataSet>();
        }

        public ListDataSet(ListDataSet lds)
        {
            dataSetList = new List<DataSet>();

            for (int i = 0; i < lds.Count; i++)
            {
                dataSetList.Add(new DataSet(lds[i]));
            }
        }

        public void Add(DataSet ds)
        {
            dataSetList.Add(ds);
        }

        public void Normalized()
        {
            //File.WriteLine(length);0
            //File.WriteLine(width);1
            //File.WriteLine(tipPositionX);2
            //File.WriteLine(tipPositionY);3
            //File.WriteLine(tipPositionZ);4
            //File.WriteLine(palmPositionX);5
            //File.WriteLine(palmPositionY);6
            //File.WriteLine(palmPositionZ);7
            //File.WriteLine(handSphereRadius);8
            //File.WriteLine(handPitch);9
            //File.WriteLine(handYaw);10
            //File.WriteLine(handRoll);11
            for (int i = 0; i < dataSetList.Count; i++)
            {
                for (int j = 0; j < dataSetList[i].AttributeCount; j++)
                {
                    if (j == 0 || j == 1 || j == 2 || j == 3)
                    {
                        dataSetList[i][j] /= 180.0f;
                    }
                    else if (j >= 4 && j <= 18)
                    {
                        dataSetList[i][j] /= 100.0f;
                    }
                        //jempol x
                    else if(j>=19 && j<=22) 
                    {
                        dataSetList[i][j] /= 200.0f;
                    }
                        //y
                    else if (j >= 23 && j <= 26)
                    {
                        dataSetList[i][j] /= 100.0f;
                    }
                        //z
                    else if (j >= 27 && j <= 30)
                    {
                        dataSetList[i][j] /= 200.0f;
                    }
                        //telunjuk + tengah x
                    else if(j==31)
                    {
                        dataSetList[i][j] /= 100.0f;
                    }
                        //y
                    else if (j == 32)
                    {
                        dataSetList[i][j] /= 100.0f;
                    }
                        //z
                    else if (j == 33)
                    {
                        dataSetList[i][j] /= 100.0f;
                    }
                    //dataSetList[i][j] /= 360f;
                    //if (j == 0 || j == 1)
                    //{
                    //    dataSetList[i][j] /= 100.0f;
                    //}
                    ////x
                    //else if (j == 2 || j == 5)
                    //{
                    //    dataSetList[i][j] = (dataSetList[i][j] + 0.0f) / (200.0f);
                    //}
                    ////y
                    //else if (j == 3 || j == 6)
                    //{
                    //    dataSetList[i][j] = (dataSetList[i][j] + 0.0f) / (400.0f);
                    //}
                    ////z
                    //else if (j == 4 || j == 7)
                    //{
                    //    dataSetList[i][j] = (dataSetList[i][j] + 0.0f) / (200.0f);
                    //}
                    //else if (j == 8 || j == 9 || j == 10 || j == 11)
                    //{
                    //    dataSetList[i][j] = (dataSetList[i][j] + 0.0f) / (360.0f);
                    //}
                    //else
                    //{
                    //    dataSetList[i][j] = (dataSetList[i][j] + 0.0f) / (100.0f);
                    //}
                }
            }
        }
    }
}
