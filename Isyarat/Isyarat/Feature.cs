﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Isyarat
{
    public class Feature
    {

        public string Keterangan { get; set; }

        public float Nilai { get; set; }

        public string Satuan { get; set; }
    }
}
