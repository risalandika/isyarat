﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using Leap;
using Vector = Leap.Vector;
using Image = System.Windows.Controls.Image;
using System.Windows;

namespace Isyarat
{
    class LeapMouse
    {
        private Image img;
        private MainWindow win = null;
        private Image GuideButtonTraining;
        
        public LeapMouse()
        {

        }

        public LeapMouse(Image img, MainWindow w)
        {
            this.img = img;
            this.win = w;
        }

        public LeapMouse(Image GuideButtonTrainin)
        {
            // TODO: Complete member initialization
            this.GuideButtonTraining = GuideButtonTraining;
            
        }

        public void Run(Vector v)
        {
            double velocity = 3.2;
            img.VerticalAlignment = VerticalAlignment.Top;
            img.HorizontalAlignment = HorizontalAlignment.Left;
            if (win != null)
            {
                img.Margin = new Thickness(v.x * velocity + win.Width / 2, v.z * velocity + win.Height / 2, 0, 0);
            }
           
        }

    }
}
