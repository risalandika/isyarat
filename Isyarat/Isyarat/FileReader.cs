﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Isyarat
{
    class FileReader
    {
        public ListDataSet ReadFile(string path, ClassificationClass cc)
        {
            path = path.TrimEnd('\\').TrimEnd('/') + "\\";
            DirectoryInfo dirInfo = new DirectoryInfo(path);
            FileInfo[] fileInfo = dirInfo.GetFiles("*.txt");

            ListDataSet dataSetList = new ListDataSet();
            foreach (FileInfo fi in fileInfo)
            {
                string className = fi.Name.Split('.')[0];
                cc.Add(className);
                StreamReader reader = new StreamReader(path + fi.Name);
                List<float> attr = new List<float>();

                while (!reader.EndOfStream)
                {
                    attr.Add(float.Parse(reader.ReadLine()));
                }

                DataSet ds = new DataSet(attr.Count);
                ds.ClassName = className;
                for (int i = 0; i < attr.Count; i++)
                {
                    //if(i==2 || i ==4 || i == 8 || i==9 || i==10 || i ==11 || i == 12 || i==13 || i==14)
                    ds[i] = attr[i];
                }

                dataSetList.Add(ds);
            }

            return dataSetList;
        }
    }
}
