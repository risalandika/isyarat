﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MessageBox = System.Windows.MessageBox;
using OpenFileDialog = Microsoft.Win32.OpenFileDialog;
using SaveFileDialog = Microsoft.Win32.SaveFileDialog;
using Leap;
using MahApps.Metro.Controls;
using Microsoft.Win32;

namespace Isyarat
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow, ILeapEventDelegate
    {

        //Leap
        private static Controller controller = new Controller();
        private static LeapEventListener listener;
        delegate void LeapEventDelegate(string EventName);

        //Method
        ClassificationClass cc = new ClassificationClass();
        Chromosom fitChrom;

        //UI
        LeapMouse leapMouse;
        float EntryButtonFarLeft, EntryButtonFarRight, EntryButtonFarBottom, EntryButtonFarTop;
        float PengenalanButtonFarLeft, PengenalanButtonFarRight, PengenalanButtonFarBottom, PengenalanButtonFarTop;
        float StartButtonFarLeft, StartButtonFarRight, StartButtonFarBottom, StartButtonFarTop;
        float StartButtonFarLeft2, StartButtonFarRight2, StartButtonFarBottom2, StartButtonFarTop2;
        float ImageTargetFarLeft, ImageTargetFarRight, ImageTargetFarBottom, ImageTargetFarTop;
        float BackToHomeButtonFarLeft, BackToHomeButtonFarRight, BackToHomeButtonFarBottom, BackToHomeButtonFarTop;
        float BackToHomeButtonFarLeft2, BackToHomeButtonFarRight2, BackToHomeButtonFarBottom2, BackToHomeButtonFarTop2;
        float ImageTargetFarLeft2, ImageTargetFarRight2, ImageTargetFarBottom2, ImageTargetFarTop2;
        float BackToTestingButtonFarLeft, BackToTestingButtonFarRight, BackToTestingButtonFarBottom, BackToTestingButtonFarTop;
        float BackToTrainingButtonFarLeft, BackToTrainingButtonFarRight, BackToTrainingButtonFarBottom, BackToTrainingButtonFarTop;
        float LogoIsyaratFarLeft, LogoIsyaratFarRight, LogoIsyaratFarBottom, LogoIsyaratFarTop;
        float gridTerjemahanHurufFarLeft, gridTerjemahanHurufFarRight, gridTerjemahanHurufFarBottom, gridTerjemahanHurufFarTop;
        float CalibrationGridFarLeft, CalibrationGridFarRight, CalibrationGridFarBottom, CalibrationGridFarTop;
        float BackToTestingButton2FarLeft, BackToTestingButton2FarRight, BackToTestingButton2FarBottom, BackToTestingButton2FarTop;
        float ImageTarget3FarLeft, ImageTarget3FarRight, ImageTarget3FarBottom, ImageTarget3FarTop;
        Stopwatch sw = new Stopwatch();
        bool goToDataTraining = false;
        bool goToDataTesting = false;
        bool isTraining = false;
        bool isTrainingON = false;
        bool isTrainingFinished = false;
        bool isTrainingOnGoing = false;
        bool backToHome2 = false;
        bool backToHome = false;
        bool backToTraining = false;
        int countGoToDataTraining = 0;
        int countGoToDataTesting = 0;
        int countOnTraining = 0;
        float idleTime = 0;
        float idleWait = 0;
        bool isClosing = false;
        string alghorithm ="BP";
        string algoTest = "BP";
        bool openFlyout = false;
        bool openFlyoutTesting = false;
        bool goToCalibrationPage = false;
        bool onCalibrating = false;
        bool backToTesting = false;
        bool isCalibratingFinished = false;
        bool isCalibratingOnGoing = false;

        float idleShape = 0;
        bool munculGambarKalibrasi = false;
        //Mode
        String mode = "home";

        //Ekstraksi fitur
        float length;
        float width;
        float tipPositionX;
        float tipPositionY;
        float tipPositionZ;
        float palmPositionX;
        float palmPositionY;
        float palmPositionZ;
        float handSphereRadius;
        float handPitch;
        float handYaw;
        float handRoll;
        float avgTipPosX = 0;
        float avgTipPosY = 0;
        float avgTipPosZ = 0;
        float avgJariPalmX;
        float avgJariPalmY;
        float avgJariPalmZ;
        float jari1PalmX;
        float jari2PalmX;
        float jari3PalmX;
        float jari4PalmX;
        float jari5PalmX;
        float jari1PalmY;
        float jari2PalmY;
        float jari3PalmY;
        float jari4PalmY;
        float jari5PalmY;
        float jari1PalmZ;
        float jari2PalmZ;
        float jari3PalmZ;
        float jari4PalmZ;
        float jari5PalmZ;
        float jari1PosX;
        float jari2PosX;
        float jari3PosX;
        float jari4PosX;
        float jari5PosX;
        float jari1PosY;
        float jari2PosY;
        float jari3PosY;
        float jari4PosY;
        float jari5PosY;
        float jari1PosZ;
        float jari2PosZ;
        float jari3PosZ;
        float jari4PosZ;
        float jari5PosZ;
        float jari0ke1X;
        float jari0ke2X;
        float jari0ke3X;
        float jari0ke4X;
        float jari0ke1Y;
        float jari0ke2Y;
        float jari0ke3Y;
        float jari0ke4Y;
        float jari0ke1Z;
        float jari0ke2Z;
        float jari0ke3Z;
        float jari0ke4Z;
        float jari1ke2X;
        float jari1ke2Y;
        float jari1ke2Z;
        float kalibrasiPanjangTangan;
        float kalibrasiLebarTangan;
        List<Feature> features = new List<Feature>();
        NeuralNetwork loadNet = new NeuralNetwork();
        
        //IO
        StreamWriter File;
        string filename="nnbaru.xml";
        int sum = 0;

        public MainWindow()
        {
            InitializeComponent();
            controller = new Controller();
            listener = new LeapEventListener(this);
            controller.AddListener(listener);
            leapMouse = new LeapMouse(GuideButton, this);

            sw.Start();
            HomeComponentLoad();
           
        }

        void HomeComponentLoad()
        {
            EntryButtonFarLeft = (float)EntryButton.Margin.Left;
            EntryButtonFarRight = (float)this.Width - (float)EntryButton.Margin.Right;
            EntryButtonFarTop = (float)EntryButton.Margin.Top;
            EntryButtonFarBottom = (float)this.Height - (float)EntryButton.Margin.Bottom;
            
            PengenalanButtonFarLeft = (float)PengenalanButton.Margin.Left;
            PengenalanButtonFarRight = (float)this.Width - (float)PengenalanButton.Margin.Right;
            PengenalanButtonFarTop = (float)PengenalanButton.Margin.Top;
            PengenalanButtonFarBottom = (float)this.Height - (float)PengenalanButton.Margin.Bottom;

            LogoIsyaratFarLeft = (float)LogoIsyarat.Margin.Left;
            LogoIsyaratFarRight = (float)this.Width - (float)LogoIsyarat.Margin.Right;
            LogoIsyaratFarTop = (float)LogoIsyarat.Margin.Top;
            LogoIsyaratFarBottom = (float)this.Height - (float)LogoIsyarat.Margin.Bottom;

            flyoutText.Text = "Aplikasi iSyarat adalah aplikasi pengenalan bahasa isyarat. Bahasa isyarat yang digunakan mengacu pada SIBI (Sistem Isyarat Bahasa Indonesia).";
        }

        void DataTrainingComponentLoad()
        {
            StartButtonFarLeft = (float)StartButton.Margin.Left;
            StartButtonFarRight = (float)this.Width - (float)StartButton.Margin.Right;
            StartButtonFarTop = (float)StartButton.Margin.Top;
            StartButtonFarBottom = (float)this.Height - (float)StartButton.Margin.Bottom;
            BackToHomeButtonFarLeft = (float)BackToHomeButton.Margin.Left;
            BackToHomeButtonFarRight = (float)this.Width - (float)BackToHomeButton.Margin.Right;
            BackToHomeButtonFarTop = (float)BackToHomeButton.Margin.Top;
            BackToHomeButtonFarBottom = (float)this.Height - (float)BackToHomeButton.Margin.Bottom;
        }

        void OnTrainingComponentLoad()
        {
            ImageTargetFarLeft = (float)imageTarget.Margin.Left;
            ImageTargetFarRight = (float)this.Width - (float)imageTarget.Margin.Right;
            ImageTargetFarTop = (float)imageTarget.Margin.Top;
            ImageTargetFarBottom = (float)this.Height - (float)imageTarget.Margin.Bottom;

            BackToTrainingButtonFarLeft = (float)BackToTrainingButton.Margin.Left;
            BackToTrainingButtonFarRight = (float)this.Width - (float)BackToTrainingButton.Margin.Right;
            BackToTrainingButtonFarTop = (float)BackToTrainingButton.Margin.Top;
            BackToTrainingButtonFarBottom = (float)this.Height - (float)BackToTrainingButton.Margin.Bottom;
        }

        void TestingComponentLoad()
        {
            StartButtonFarLeft2 = (float)StartButton2.Margin.Left;
            StartButtonFarRight2 = (float)this.Width - (float)StartButton2.Margin.Right;
            StartButtonFarTop2 = (float)StartButton2.Margin.Top;
            StartButtonFarBottom2 = (float)this.Height - (float)StartButton2.Margin.Bottom;

            BackToHomeButtonFarLeft2 = (float)BackToHomeButton2.Margin.Left;
            BackToHomeButtonFarRight2 = (float)this.Width - (float)BackToHomeButton2.Margin.Right;
            BackToHomeButtonFarTop2 = (float)BackToHomeButton2.Margin.Top;
            BackToHomeButtonFarBottom2 = (float)this.Height - (float)BackToHomeButton2.Margin.Bottom;

            gridTerjemahanHurufFarLeft = (float)btnTemp.Margin.Left;
            gridTerjemahanHurufFarRight = (float)this.Width - (float)btnTemp.Margin.Right;
            gridTerjemahanHurufFarTop = (float)btnTemp.Margin.Top;
            gridTerjemahanHurufFarBottom = (float)this.Height - (float)btnTemp.Margin.Bottom;

            CalibrationGridFarLeft = (float)CalibrationGrid.Margin.Left;
            CalibrationGridFarRight = (float)this.Width - (float)CalibrationGrid.Margin.Right;
            CalibrationGridFarTop = (float)CalibrationGrid.Margin.Top;
            CalibrationGridFarBottom = (float)this.Height - (float)CalibrationGrid.Margin.Bottom;
        }

        void OnTestingComponentLoad()
        {
            ImageTargetFarLeft2 = (float)imageTarget2.Margin.Left;
            ImageTargetFarRight2 = (float)this.Width - (float)imageTarget2.Margin.Right;
            ImageTargetFarTop2 = (float)imageTarget2.Margin.Top;
            ImageTargetFarBottom2 = (float)this.Height - (float)imageTarget2.Margin.Bottom;

            BackToTestingButtonFarLeft = (float)BackToTestingButton.Margin.Left;
            BackToTestingButtonFarRight = (float)this.Width - (float)BackToTestingButton.Margin.Right;
            BackToTestingButtonFarTop = (float)BackToTestingButton.Margin.Top;
            BackToTestingButtonFarBottom = (float)this.Height - (float)BackToTestingButton.Margin.Bottom;
        }

        void CalibratingComponentLoad()
        {
            ImageTarget3FarLeft = (float)ImageTarget3.Margin.Left;
            ImageTarget3FarRight = (float)this.Width - (float)ImageTarget3.Margin.Right;
            ImageTarget3FarTop = (float)ImageTarget3.Margin.Top;
            ImageTarget3FarBottom = (float)this.Height - (float)ImageTarget3.Margin.Bottom;

            BackToTestingButton2FarLeft = (float)BackToTestingButton2.Margin.Left;
            BackToTestingButton2FarRight = (float)this.Width - (float)BackToTestingButton2.Margin.Right;
            BackToTestingButton2FarTop = (float)BackToTestingButton2.Margin.Top;
            BackToTestingButton2FarBottom = (float)this.Height - (float)BackToTestingButton2.Margin.Bottom;
        }

        public void LeapEventNotification(string EventName)
        {
            if (this.CheckAccess())
            {
                switch (EventName)
                {
                    case "onInit":
                        Console.WriteLine("Init");  
                        break;
                    case "onConnect":
                        Console.WriteLine("Leap Motion is connected...");  
                        this.connectHandler();
                        break;
                    case "onFrame":
                        if (!this.isClosing)
                        {
                            sw.Stop();
                            this.newFrameHandler(controller.Frame(), sw.ElapsedMilliseconds / 1000.000f);
                            sw.Reset();
                            sw.Start();
                        }
                        break;
                    case "onDisconnect":
                        Console.WriteLine("Leap motion is disconnected...");
                        break;
                    case "onExit":
                        Console.WriteLine("Leap motion is exiting...");
                        break;
                }
            }
            else
            {
                Dispatcher.Invoke(new LeapEventDelegate(LeapEventNotification), new object[] { EventName });
            }
        }

        void connectHandler()
        {
        }

        void newFrameHandler(Leap.Frame frame, float deltaTime)
        {
            if (frame.Hands.Count != 0)
            {
                Console.WriteLine(mode);
                leapMouse.Run(frame.Hands.Rightmost.PalmPosition);
                if (mode.Equals("home"))
                {
                    homeMode(deltaTime);
                }
                else if (mode.Equals("training"))
                {
                    DataTrainingComponentLoad();
                    TrainingMode(frame, deltaTime);
                }
                else if (mode.Equals("onTraining"))
                {
                    OnTrainingComponentLoad();
                    OnTrainingMode(frame, deltaTime);
                }
                else if (mode.Equals("testing"))
                {
                    TestingComponentLoad();
                    TestingMode(frame, deltaTime);
                }
                else if (mode.Equals("onTesting"))
                {
                    OnTestingComponentLoad();
                    OnTestingMode(frame, deltaTime);
                }
                else if (mode.Equals("calibrating"))
                {
                    CalibratingComponentLoad();
                    CalibrateMode(frame, deltaTime);
                }
            }
        }

       
        void homeMode(float deltaTime)
        {
            if (GuideButton.Margin.Left <= EntryButtonFarRight && GuideButton.Margin.Left >= EntryButtonFarLeft
                        && GuideButton.Margin.Top <= EntryButtonFarBottom && GuideButton.Margin.Top >= EntryButtonFarTop && mode.Equals("home"))
            {
                goToDataTraining = true;
                EntryButton.Background = Brushes.Lavender;
                EntryButton.Foreground = Brushes.Teal;
                EntryButton.Width += 4;
                EntryButton.Height += 4;
                idleTime += deltaTime;
                if (PengenalanButton.Width >= 250f || PengenalanButton.Height >= 250f)
                {
                    PengenalanButton.Width -= 5;
                    PengenalanButton.Height -= 5;
                }
            }
            else if (GuideButton.Margin.Left <= PengenalanButtonFarRight && GuideButton.Margin.Left >= PengenalanButtonFarLeft
                  && GuideButton.Margin.Top <= PengenalanButtonFarBottom && GuideButton.Margin.Top >= PengenalanButtonFarTop && mode.Equals("home"))
            {
                PengenalanButton.Background = Brushes.Lavender;
                PengenalanButton.Foreground = Brushes.Teal;
                PengenalanButton.Width +=  4;
                PengenalanButton.Height +=  4;
                idleTime += deltaTime;
                goToDataTesting = true;
                if (EntryButton.Width >= 250f || EntryButton.Height >= 250f)
                {
                    EntryButton.Width -= 5;
                    EntryButton.Height -= 5;
                }
            }
            else if (GuideButton.Margin.Left <= LogoIsyaratFarRight && GuideButton.Margin.Left >= LogoIsyaratFarLeft
                  && GuideButton.Margin.Top <= LogoIsyaratFarBottom && GuideButton.Margin.Top >= LogoIsyaratFarTop && mode.Equals("home"))
            {
                idleTime += deltaTime;
                openFlyout = true;
                if (PengenalanButton.Width >= 250f || PengenalanButton.Height >= 250f)
                {
                    PengenalanButton.Width -= 5;
                    PengenalanButton.Height -= 5;
                }
                if (EntryButton.Width >= 250f || EntryButton.Height >= 250f)
                {
                    EntryButton.Width -= 5;
                    EntryButton.Height -= 5;
                }
            }
            else
            {
                PengenalanButton.Background = Brushes.Teal;
                PengenalanButton.Foreground = Brushes.Lavender;
                EntryButton.Background = Brushes.Teal;
                EntryButton.Foreground = Brushes.Lavender;
                goToDataTraining = false;
                goToDataTesting = false;
                flyout.IsOpen = false;
                openFlyout = false;
                if (PengenalanButton.Width >= 250f || PengenalanButton.Height >= 250f)
                {
                    PengenalanButton.Width -= 5;
                    PengenalanButton.Height -= 5;
                }
                if (EntryButton.Width >= 250f || EntryButton.Height >= 250f)
                {
                    EntryButton.Width -= 5;
                    EntryButton.Height -= 5;
                }
                idleTime = 0;
            }
            if (goToDataTraining && idleTime >= 1.0 && countGoToDataTraining == 0)
            {
                countGoToDataTraining++;
                GridHome.Visibility = Visibility.Hidden;
                GridDataTraining.Visibility = Visibility.Visible;
                mode = "training";
            }
            else if (goToDataTesting && idleTime >= 1.0 && countGoToDataTesting == 0)
            {
                countGoToDataTesting++;
                GridHome.Visibility = Visibility.Hidden;
                GridDataTesting.Visibility = Visibility.Visible;
                mode = "testing";
            }
            else if (openFlyout && idleTime >= 0.3)
            {
                flyout.IsOpen = true;
                System.Windows.Controls.Panel.SetZIndex(LogoIsyarat, -20);
                // LogoIsyarat.Visibility = Visibility.Hidden;
            }
        }

        void TestingMode(Leap.Frame frame, float deltaTime)
        {
            PanjangTanganTextBlock.Text = "Panjang: " + kalibrasiPanjangTangan.ToString() + " mm";
            LebarTanganTextBlock.Text = "Lebar: " + kalibrasiLebarTangan.ToString() + " mm";
            if (Keyboard.IsKeyDown(Key.Q))
            {
                filename = "aaaa.xml";
            }
            else if(Keyboard.IsKeyDown(Key.W))
            {
                filename = "aaaa.xml";
            }
            GridDataTesting.Visibility = Visibility.Visible;

            GridCalibrating.Visibility = Visibility.Hidden;
            txtNN.Text = filename + " is loaded successfully.";
            if (GuideButton.Margin.Left <= StartButtonFarRight2 && GuideButton.Margin.Left >= StartButtonFarLeft2
                       && GuideButton.Margin.Top <= StartButtonFarBottom2 && GuideButton.Margin.Top >= StartButtonFarTop2 && mode.Equals("testing"))
            {
                StartButton2Img.Width += 4;
                StartButton2Img.Height += 4;
                if (BackToHomeButton2Img.Width >= 100 || BackToHomeButton2Img.Height >= 100)
                {
                    BackToHomeButton2Img.Width -= 5;
                    BackToHomeButton2Img.Height -= 5;
                }
                if (CalibrationButton.Width >= 100 || CalibrationButton.Height >= 100)
                {
                    CalibrationButton.Width -= 5;
                    CalibrationButton.Height -= 5;
                }
                isTraining = true;

                goToCalibrationPage = false;
                idleTime += deltaTime;

            }
            else if (GuideButton.Margin.Left <= BackToHomeButtonFarRight2 && GuideButton.Margin.Left >= BackToHomeButtonFarLeft2
                       && GuideButton.Margin.Top <= BackToHomeButtonFarBottom2 && GuideButton.Margin.Top >= BackToHomeButtonFarTop2 && mode.Equals("testing"))
            {
                BackToHomeButton2Img.Width += 4;
                BackToHomeButton2Img.Height += 4;
                if (StartButton2Img.Width >= 200 || StartButton2Img.Height >= 200)
                {
                    StartButton2Img.Width -= 5;
                    StartButton2Img.Height -= 5;
                }
                if (CalibrationButton.Width >= 100 || CalibrationButton.Height >= 100)
                {
                    CalibrationButton.Width -= 5;
                    CalibrationButton.Height -= 5;
                }
                backToHome2 = true;
                idleTime += deltaTime;
            }
            else if (GuideButton.Margin.Left <= gridTerjemahanHurufFarRight && GuideButton.Margin.Left >= gridTerjemahanHurufFarLeft
                      && GuideButton.Margin.Top <= gridTerjemahanHurufFarBottom && GuideButton.Margin.Top >= gridTerjemahanHurufFarTop && mode.Equals("testing"))
            {
                 openFlyoutTesting = true;
                 idleTime += deltaTime;
            }
            else if (GuideButton.Margin.Left <= CalibrationGridFarRight && GuideButton.Margin.Left >= CalibrationGridFarLeft
                  && GuideButton.Margin.Top <= CalibrationGridFarBottom && GuideButton.Margin.Top >= CalibrationGridFarTop && mode.Equals("testing"))
            {
                idleTime += deltaTime; 
                CalibrationButton.Width += 4;
                CalibrationButton.Height += 4;
                goToCalibrationPage = true;
            }
            else
            {
              
                idleTime = 0;
                StartButton2.Opacity = 1;
                BackToHomeButton2.Opacity = 1;
                isTraining = false;
                openFlyoutTesting = false;
                backToHome2 = false;
                goToCalibrationPage = false;
                if (BackToHomeButton2Img.Width >= 100 || BackToHomeButton2Img.Height >= 100)
                {
                    BackToHomeButton2Img.Width -= 5;
                    BackToHomeButton2Img.Height -= 5;
                }
                if (StartButton2Img.Width >= 200 || StartButton2Img.Height >= 200)
                {
                    StartButton2Img.Width -= 5;
                    StartButton2Img.Height -= 5;
                }
                if (CalibrationButton.Width >= 100 || CalibrationButton.Height >= 100)
                {
                    CalibrationButton.Width -= 5;
                    CalibrationButton.Height -= 5;
                }
            }
            if (backToHome2 && idleTime > 1.0)
            {
                GridDataTesting.Visibility = Visibility.Hidden;
                GridHome.Visibility = Visibility.Visible;
                backToHome2 = false;
                countGoToDataTesting = 0;
                mode = "home";
            }
            if (goToCalibrationPage && idleTime > 1.0)
            {
                GridDataTesting.Visibility = Visibility.Hidden;
                GridCalibrating.Visibility = Visibility.Visible;
                ImageTarget3.Visibility = Visibility.Visible;
                progressRing3.Visibility = Visibility.Hidden;
                mode = "calibrating";
            }
            if (openFlyoutTesting && idleTime > 0.3)
            {
                flyoutTerjemah.IsOpen = true;
            }
            if (isTraining && idleTime > 1.0)
            {
                GridDataTesting.Visibility = Visibility.Hidden;
                GridDataTestingON.Visibility = Visibility.Visible;
                GridHome.Visibility = Visibility.Hidden;
                imageTarget2.Visibility = Visibility.Visible;
                progressRing2.Visibility = Visibility.Hidden;
                countOnTraining = 0;
                labelFrame2.Content = countOnTraining.ToString() + " frame telah disimpan..";

                length = 0;
                width = 0;
                tipPositionX = 0;
                tipPositionY = 0;
                tipPositionZ = 0;
                palmPositionX = 0;
                palmPositionY = 0;
                palmPositionZ = 0;
                handSphereRadius = 0;
                handPitch = 0;
                handYaw = 0;
                handRoll = 0;
                avgTipPosX = 0;
                avgTipPosY = 0;
                avgTipPosZ = 0;
                jari1PalmX = 0;
                jari2PalmX = 0;
                jari3PalmX = 0;
                jari4PalmX = 0;
                jari5PalmX = 0;
                jari1PalmY = 0;
                jari2PalmY = 0;
                jari3PalmY = 0;
                jari4PalmY = 0;
                jari5PalmY = 0;
                jari1PalmZ = 0;
                jari2PalmZ = 0;
                jari3PalmZ = 0;
                jari4PalmZ = 0;
                jari5PalmZ = 0;
                jari1PosX = 0;
                jari2PosX = 0;
                jari3PosX = 0;
                jari4PosX = 0;
                jari5PosX = 0;
                jari1PosY = 0;
                jari2PosY = 0;
                jari3PosY = 0;
                jari4PosY = 0;
                jari5PosY = 0;
                jari1PosZ = 0;
                jari2PosZ = 0;
                jari3PosZ = 0;
                jari4PosZ = 0;
                jari5PosZ = 0;
                jari0ke1X= 0;
                jari0ke2X= 0;
                jari0ke3X= 0;
                jari0ke4X= 0;
                jari0ke1Y= 0;
                jari0ke2Y= 0;
                jari0ke3Y= 0;
                jari0ke4Y= 0;
                jari0ke1Z= 0;
                jari0ke2Z= 0;
                jari0ke3Z= 0;
                jari0ke4Z= 0;
                jari1ke2X= 0;
                jari1ke2Y= 0;
                jari1ke2Z= 0;
                avgJariPalmX = 0;
                avgJariPalmY = 0;
                avgJariPalmX = 0;
                isTrainingFinished = false;
                isTraining = false;
                mode = "onTesting";

            }
        }

        void OnTestingMode(Leap.Frame frame, float deltaTime)
        {
            GridCalibrating.Visibility = Visibility.Hidden;
            float countdownStart = 4;
            
            if (GuideButton.Margin.Left <= ImageTargetFarRight2 && GuideButton.Margin.Left >= ImageTargetFarLeft2
                      && GuideButton.Margin.Top <= ImageTargetFarBottom2 && GuideButton.Margin.Top >= ImageTargetFarTop2 && mode.Equals("onTesting"))
            {
                idleTime += deltaTime;
                if (frame.Hands[0].PalmPosition.y >= 400.0f && !isTrainingON)
                    labelCursor2.Content = "Dekatkan telapak anda ke Leap Motion";
                else if (frame.Hands[0].PalmPosition.y < 100.0f && !isTrainingON)
                    labelCursor2.Content = "Jauhkan telapak anda dari Leap Motion";
                else
                {
                    isTrainingON = true;
                }
            }
            else if (GuideButton.Margin.Left <= BackToTestingButtonFarRight && GuideButton.Margin.Left >= BackToTestingButtonFarLeft
                       && GuideButton.Margin.Top <= BackToTestingButtonFarBottom && GuideButton.Margin.Top >= BackToTestingButtonFarTop && mode.Equals("onTesting"))
            {
                backToHome2 = true;
                idleTime += deltaTime;
                BackToTestingButton.Opacity = 0.25;
            }
            else 
            {
                isTrainingON = false;
                isTrainingOnGoing = false;
                backToHome2 = false;
                labelCursor2.Content = "gerakkan cursor sampai berada di area bulat berwarna merah";
                imageTarget2.Source = new BitmapImage(new Uri(@"/Images/target-red.png", UriKind.Relative));
                idleTime = 0;
                BackToTestingButton.Opacity = 1;
            }
            if (backToHome2 && idleTime > 1.0)
            {
                GridDataTesting.Visibility = Visibility.Visible;
                GridDataTestingON.Visibility = Visibility.Hidden;
                mode = "testing";
            }
            if (isTrainingON && idleTime >= 1.0 && (countdownStart - idleTime) >= 0)
            {
                labelCursor2.Content = "menyimpan isyarat dalam " + (countdownStart - idleTime).ToString("0") + "...";
            }
            if ((countdownStart - idleTime) <= 2.0 && isTrainingON)
            {
                imageTarget2.Source = new BitmapImage(new Uri(@"/Images/target-yellow.png", UriKind.Relative));
            }
            if ((countdownStart - idleTime) <= 1.0 && isTrainingON)
            {
                imageTarget2.Source = new BitmapImage(new Uri(@"/Images/target-green.png", UriKind.Relative));
            }

            if ((countdownStart - idleTime) <= 0 && !isTrainingFinished && isTrainingON)
            {
                labelCursor2.Content = "tunggu beberapa saat...";
                isTrainingOnGoing = true;
            }

            if (isTrainingOnGoing)
            {
                Console.WriteLine("menyimpan isyarat...");
                Pointable pointable = frame.Pointables.Frontmost;
                HandList hands = frame.Hands;
                Hand hand = hands[0];


                /*
                float totalTipPosX = 0, totalTipPosY = 0, totalTipPosZ = 0;
                for (int i = 0; i < 5; i++)
                {
                    totalTipPosX += hand.Fingers[i].TipPosition.x;
                    totalTipPosY += hand.Fingers[i].TipPosition.y;
                    totalTipPosZ += hand.Fingers[i].TipPosition.z;
                }

                totalTipPosX /= 5;
                totalTipPosY /= 5;
                totalTipPosZ /= 5;
                avgTipPosX += totalTipPosX;
                avgTipPosY += totalTipPosY;
                avgTipPosZ += totalTipPosZ;
                */

                /*
                length += pointable.Length;
                width += pointable.Width;
                tipPositionX += avgTipPosX;
                tipPositionY += avgTipPosY;
                tipPositionZ += avgTipPosZ;
                palmPositionX += hand.PalmPosition.x;
                palmPositionY += hand.PalmPosition.y;
                palmPositionZ += hand.PalmPosition.z;
                avgJariPalmX += (jari1PalmX + jari2PalmX + jari3PalmX + jari4PalmX + jari5PalmX) / 5.0f;
                avgJariPalmY += (jari1PalmY + jari2PalmY + jari3PalmY + jari4PalmY + jari5PalmY) / 5.0f;
                avgJariPalmZ += (jari1PalmZ + jari2PalmZ + jari3PalmZ + jari4PalmZ + jari5PalmZ) / 5.0f;
                */
                handSphereRadius += hand.SphereRadius;
                handPitch += Math.Abs(hand.Direction.Pitch * (float)(180.0 / Math.PI));
                handYaw += Math.Abs(hand.Direction.Yaw * (float)(180.0 / Math.PI));
                handRoll += Math.Abs(hand.Direction.Roll * (float)(180.0 / Math.PI));

                jari1PosX += hand.Fingers[0].TipPosition.x;
                jari2PosX += hand.Fingers[1].TipPosition.x;
                jari3PosX += hand.Fingers[2].TipPosition.x;
                jari4PosX += hand.Fingers[3].TipPosition.x;
                jari5PosX += hand.Fingers[4].TipPosition.x;
                jari1PosY += hand.Fingers[0].TipPosition.y;
                jari2PosY += hand.Fingers[1].TipPosition.y;
                jari3PosY += hand.Fingers[2].TipPosition.y;
                jari4PosY += hand.Fingers[3].TipPosition.y;
                jari5PosY += hand.Fingers[4].TipPosition.y;
                jari1PosZ += hand.Fingers[0].TipPosition.z;
                jari2PosZ += hand.Fingers[1].TipPosition.z;
                jari3PosZ += hand.Fingers[2].TipPosition.z;
                jari4PosZ += hand.Fingers[3].TipPosition.z;
                jari5PosZ += hand.Fingers[4].TipPosition.z;

                jari1PalmX += (hand.Fingers[0].TipPosition.x - hand.PalmPosition.x);
                jari2PalmX += (hand.Fingers[1].TipPosition.x - hand.PalmPosition.x);
                jari3PalmX += (hand.Fingers[2].TipPosition.x - hand.PalmPosition.x);
                jari4PalmX += (hand.Fingers[3].TipPosition.x - hand.PalmPosition.x);
                jari5PalmX += (hand.Fingers[4].TipPosition.x - hand.PalmPosition.x);
                jari1PalmY += (hand.Fingers[0].TipPosition.y - hand.PalmPosition.y);
                jari2PalmY += (hand.Fingers[1].TipPosition.y - hand.PalmPosition.y);
                jari3PalmY += (hand.Fingers[2].TipPosition.y - hand.PalmPosition.y);
                jari4PalmY += (hand.Fingers[3].TipPosition.y - hand.PalmPosition.y);
                jari5PalmY += (hand.Fingers[4].TipPosition.y - hand.PalmPosition.y);
                jari1PalmZ += (hand.Fingers[0].TipPosition.z - hand.PalmPosition.z);
                jari2PalmZ += (hand.Fingers[1].TipPosition.z - hand.PalmPosition.z);
                jari3PalmZ += (hand.Fingers[2].TipPosition.z - hand.PalmPosition.z);
                jari4PalmZ += (hand.Fingers[3].TipPosition.z - hand.PalmPosition.z);
                jari5PalmZ += (hand.Fingers[4].TipPosition.z - hand.PalmPosition.z);

                jari0ke1X += hand.Fingers[1].TipPosition.x - hand.Fingers[0].TipPosition.x;
                jari0ke2X += hand.Fingers[2].TipPosition.x - hand.Fingers[0].TipPosition.x;
                jari0ke3X += hand.Fingers[3].TipPosition.x - hand.Fingers[0].TipPosition.x;
                jari0ke4X += hand.Fingers[4].TipPosition.x - hand.Fingers[0].TipPosition.x;
                jari0ke1Y += hand.Fingers[1].TipPosition.y - hand.Fingers[0].TipPosition.y;
                jari0ke2Y += hand.Fingers[2].TipPosition.y - hand.Fingers[0].TipPosition.y;
                jari0ke3Y += hand.Fingers[3].TipPosition.y - hand.Fingers[0].TipPosition.y;
                jari0ke4Y += hand.Fingers[4].TipPosition.y - hand.Fingers[0].TipPosition.y;
                jari0ke1Z += hand.Fingers[1].TipPosition.z - hand.Fingers[0].TipPosition.z;
                jari0ke2Z += hand.Fingers[2].TipPosition.z - hand.Fingers[0].TipPosition.z;
                jari0ke3Z += hand.Fingers[3].TipPosition.z - hand.Fingers[0].TipPosition.z;
                jari0ke4Z += hand.Fingers[4].TipPosition.z - hand.Fingers[0].TipPosition.z;
                jari1ke2X += hand.Fingers[2].TipPosition.x - hand.Fingers[1].TipPosition.x;
                jari1ke2Y += hand.Fingers[2].TipPosition.y - hand.Fingers[1].TipPosition.y;
                jari1ke2Z += hand.Fingers[2].TipPosition.z - hand.Fingers[1].TipPosition.z;

                countOnTraining++;
                labelFrame2.Content = countOnTraining.ToString() + " frame telah disimpan..";
                if (countOnTraining == 10)
                {
                    /*
                    length /= countOnTraining;
                    width /= countOnTraining;
                    tipPositionX /= countOnTraining;
                    tipPositionY /= countOnTraining;
                    tipPositionZ /= countOnTraining;
                    palmPositionX /= countOnTraining;
                    palmPositionY /= countOnTraining;
                    palmPositionZ /= countOnTraining;
                    avgTipPosX /= countOnTraining;
                    avgTipPosY /= countOnTraining;
                    avgTipPosZ /= countOnTraining;
                    avgJariPalmX /= countOnTraining;
                    avgJariPalmY /= countOnTraining;
                    avgJariPalmZ /= countOnTraining;
                    */

                    handSphereRadius /= countOnTraining;
                    handPitch /= countOnTraining;
                    handYaw /= countOnTraining;
                    handRoll /= countOnTraining;
                    jari1PalmX /= countOnTraining;
                    jari2PalmX /= countOnTraining;
                    jari3PalmX /= countOnTraining;
                    jari4PalmX /= countOnTraining;
                    jari5PalmX /= countOnTraining;
                    jari1PalmY /= countOnTraining;
                    jari2PalmY /= countOnTraining;
                    jari3PalmY /= countOnTraining;
                    jari4PalmY /= countOnTraining;
                    jari5PalmY /= countOnTraining;
                    jari1PalmZ /= countOnTraining;
                    jari2PalmZ /= countOnTraining;
                    jari3PalmZ /= countOnTraining;
                    jari4PalmZ /= countOnTraining;
                    jari5PalmZ /= countOnTraining;
                    jari1PosX /= countOnTraining;
                    jari2PosX /= countOnTraining;
                    jari3PosX /= countOnTraining;
                    jari4PosX /= countOnTraining;
                    jari5PosX /= countOnTraining;
                    jari1PosY /= countOnTraining;
                    jari2PosY /= countOnTraining;
                    jari3PosY /= countOnTraining;
                    jari4PosY /= countOnTraining;
                    jari5PosY /= countOnTraining;
                    jari1PosZ /= countOnTraining;
                    jari2PosZ /= countOnTraining;
                    jari3PosZ /= countOnTraining;
                    jari4PosZ /= countOnTraining;
                    jari5PosZ /= countOnTraining;
                    jari0ke1X /= countOnTraining;
                    jari0ke2X /= countOnTraining;
                    jari0ke3X /= countOnTraining;
                    jari0ke4X /= countOnTraining;
                    jari0ke1Y /= countOnTraining;
                    jari0ke2Y /= countOnTraining;
                    jari0ke3Y /= countOnTraining;
                    jari0ke4Y /= countOnTraining;
                    jari0ke1Z /= countOnTraining;
                    jari0ke2Z /= countOnTraining;
                    jari0ke3Z /= countOnTraining;
                    jari0ke4Z /= countOnTraining;
                    jari1ke2X /= countOnTraining;
                    jari1ke2Y /= countOnTraining;
                    jari1ke2Z /= countOnTraining;

                    isTrainingFinished = true;
                    isTrainingOnGoing = false;
                
                    ListDataSet dsl = new ListDataSet();
                    List<float> fitur = new List<float>();
                    //fitur.Add(length);
                    //fitur.Add(width);
                    //fitur.Add(avgTipPosX);
                    //fitur.Add(avgTipPosY);
                    //fitur.Add(avgTipPosZ);
                    //fitur.Add(palmPositionX);
                    //fitur.Add(palmPositionY);
                    //fitur.Add(palmPositionZ);
                    //fitur.Add(handSphereRadius);
                    //fitur.Add(handPitch);
                    //fitur.Add(handYaw);
                    //fitur.Add(handRoll);
                    //fitur.Add(avgJariPalmX);
                    //fitur.Add(avgJariPalmY);
                    //fitur.Add(avgJariPalmZ);
                    fitur.Add(handSphereRadius);
                    fitur.Add(handPitch);
                    fitur.Add(handYaw);
                    fitur.Add(handRoll);
                    fitur.Add(jari1PalmX);
                    fitur.Add(jari2PalmX);
                    fitur.Add(jari3PalmX);
                    fitur.Add(jari4PalmX);
                    fitur.Add(jari5PalmX);
                    fitur.Add(jari1PalmY);
                    fitur.Add(jari2PalmY);
                    fitur.Add(jari3PalmY);
                    fitur.Add(jari4PalmY);
                    fitur.Add(jari5PalmY);
                    fitur.Add(jari1PalmZ);
                    fitur.Add(jari2PalmZ);
                    fitur.Add(jari3PalmZ);
                    fitur.Add(jari4PalmZ);
                    fitur.Add(jari5PalmZ);
                    fitur.Add(jari0ke1X);
                    fitur.Add(jari0ke2X);
                    fitur.Add(jari0ke3X);
                    fitur.Add(jari0ke4X);
                    fitur.Add(jari0ke1Y);
                    fitur.Add(jari0ke2Y);
                    fitur.Add(jari0ke3Y);
                    fitur.Add(jari0ke4Y);
                    fitur.Add(jari0ke1Z);
                    fitur.Add(jari0ke2Z);
                    fitur.Add(jari0ke3Z);
                    fitur.Add(jari0ke4Z);
                    fitur.Add(jari1ke2X);
                    fitur.Add(jari1ke2Y);
                    fitur.Add(jari1ke2Z);

                    filename = "gol1_v4_10k.xml";
                    sum = 0;
                    
                    if (jari2PalmZ <= -70.0f)
                    {
                        filename = "gol3_v3_15k.xml";
                        sum = 2;
                        Console.WriteLine("jari2palmz");
                    }
                    if (jari3PalmZ <= -70.0f)
                    {
                        filename = "gol3_v3_15k.xml";
                        sum = 2;
                        Console.WriteLine("jari3palmz");
                    }
                    if (jari1PalmX <= -42.5f)
                    {
                        filename = "gol2_v3_10k.xml";
                        sum = 1;
                        Console.WriteLine("jari1palmx");
                    }
                    if (jari3PalmX <= -27.0f)
                    {
                        filename = "gol2_v3_10k.xml";
                        sum = 1;
                        Console.WriteLine("jari3palmx");                     
                    }
                    if (jari1PalmX <= -45.0f)
                    {
                        filename = "gol1_v4_10k.xml";
                        sum = 1;
                    }

                    /*
                    if (fitur[5] <= -25.0f )
                    {
                        filename = "gol2_v2_5k.xml";
                        sum = 1;
                        Console.WriteLine("masuk sini");
                    }
                    else if (fitur[24] >= fitur[26] && fitur[24] >= fitur[25] && fitur[23] >= fitur[26])
                    {
                        filename = "gol2_v2_5k.xml";
                        sum = 1;
                        Console.WriteLine("masuk situ");
                    }
                    else
                    {
                        filename = "gol1_v2_10k.xml";
                        sum = 0;
                    }
                    if (fitur[15] <= -60.0f)
                    {
                        filename = "gol3_v2_10k.xml";

                        Console.WriteLine("masuk sono");
                        sum = 2;
                    }
                    else if (fitur[15] >= -25.0f && fitur[15] <=0.0f && fitur[16] <= -50.0f)
                    {
                        filename = "gol3_v2_10k.xml";

                        Console.WriteLine("masuk sinu");
                        sum = 2;
                    }
                    else 
                    {
                        filename = "gol1_v2_10k.xml";
                        sum = 0;
                    }
                     */

                    DataSet ds = new DataSet(fitur.Count);
                    for (int i = 0; i < fitur.Count; i++)
                    {
                        ds[i] = fitur[i];
                        //Console.WriteLine(fitur[i]);
                    }

                    dsl.Add(ds);
                    dsl.Normalized();

                    FeedForward ff = new FeedForward();
                    loadNet = new NeuralNetwork();
                    NNtoXMLReader nnr = new NNtoXMLReader(loadNet);
                    float[] totalSelisih = new float[3];
                    Translate t = new Translate();

                    float totalselisihtemp = 0;
                    //1

                    //filename = "gol1_v2_10k.xml";

                    //for (int i = 0; i < dsl.Count; i++)
                    //{
                    //    if (algo.Equals("GABP"))
                    //    {
                    //        int popCount = 0;
                    //        for (int j = 0; j < nnr.chromosom.Length; j++)
                    //        {
                    //            if (nnr.chromosom[j] == 0)
                    //            {
                    //                dsl[i].RemoveBit(j - popCount);
                    //                popCount++;
                    //            }
                    //        }
                    //    }
                    //    ff.Initialise(loadNet, dsl);
                    //    ff.Run(i);
                    //}

                    //for (int j = 0; j < loadNet.OutputLayer.Count; j++)
                    //{
                    //    //totalselisihtemp = Math.Abs(loadNet.OutputLayer[j].Input - loadNet.treshold);
                    //    //if (totalselisihtemp >= 0.5) totalselisihtemp = 0;
                    //    totalselisihtemp = loadNet.OutputLayer[j].Input;
                    //    if (totalselisihtemp <= 0.0f) totalselisihtemp = 0.5f;
                    //    else if (totalselisihtemp >= 1.0f) totalselisihtemp = 0.5f;
                    //    totalSelisih[0] += Math.Abs(totalselisihtemp - loadNet.treshold);
                    //}

                    /*
                    filename = "gol1_v2_5k.xml";
                    loadNet = new NeuralNetwork();
                    nnr = new NNtoXMLReader(loadNet);
                    nnr.read(filename);

                    ff.Initialise(loadNet, dsl);
                    for (int i = 0; i < dsl.Count; i++)
                    {
                        ff.Run(i);
                    }

                    for (int j = 0; j < loadNet.OutputLayer.Count; j++)
                    {
                        //totalselisihtemp = Math.Abs(loadNet.OutputLayer[j].Input - loadNet.treshold);
                        //if (totalselisihtemp >= 0.5) totalselisihtemp = 0;
                        totalselisihtemp = loadNet.OutputLayer[j].Input;
                        //Console.WriteLine(totalselisihtemp);
                        if (totalselisihtemp <= 0.0f) totalselisihtemp = 0.5f;
                        else if (totalselisihtemp >= 1.0f) totalselisihtemp = 0.5f;
                        totalSelisih[0] += Math.Abs(totalselisihtemp - loadNet.treshold);
                    }


                    filename = "gol2_v2_5k.xml";
                    loadNet = new NeuralNetwork();
                    nnr = new NNtoXMLReader(loadNet);
                    nnr.read(filename);

                    ff.Initialise(loadNet, dsl);
                    for (int i = 0; i < dsl.Count; i++)
                    {
                        ff.Run(i);
                    }

                    for (int j = 0; j < loadNet.OutputLayer.Count; j++)
                    {
                        //totalselisihtemp = Math.Abs(loadNet.OutputLayer[j].Input - loadNet.treshold);
                        //if (totalselisihtemp >= 0.5) totalselisihtemp = 0;
                        totalselisihtemp = loadNet.OutputLayer[j].Input;
                        //Console.WriteLine(totalselisihtemp);
                        if (totalselisihtemp <= 0.0f) totalselisihtemp = 0.5f;
                        else if (totalselisihtemp >= 1.0f) totalselisihtemp = 0.5f;
                        totalSelisih[1] += Math.Abs(totalselisihtemp - loadNet.treshold);
                    }

                    //3
                    filename = "gol3_v2_10k.xml";
                    loadNet = new NeuralNetwork();
                    nnr = new NNtoXMLReader(loadNet);
                    nnr.read(filename);

                    ff.Initialise(loadNet, dsl);
                    for (int i = 0; i < dsl.Count; i++)
                    {
                        ff.Run(i);
                    }

                    for (int j = 0; j < loadNet.OutputLayer.Count; j++)
                    {
                        //totalselisihtemp = Math.Abs(loadNet.OutputLayer[j].Input - loadNet.treshold);
                        //if (totalselisihtemp >= 0.5) totalselisihtemp = 0;
                        totalselisihtemp = loadNet.OutputLayer[j].Input;
                        ////Console.WriteLine(totalselisihtemp);
                        if (totalselisihtemp <= 0.0f) totalselisihtemp = 0.5f;
                        else if (totalselisihtemp >= 1.0f) totalselisihtemp = 0.5f;
                        totalSelisih[2] += Math.Abs(totalselisihtemp - loadNet.treshold);
                    }

                    if (totalSelisih[0] > totalSelisih[1] && totalSelisih[0] > totalSelisih[2])
                    {
                        filename = "gol1_v2_5k.xml";
                        sum = 0;
                    }
                    else if (totalSelisih[1] > totalSelisih[0] && totalSelisih[1] > totalSelisih[2])
                    {
                        filename = "gol2_v2_5k.xml";
                        sum = 1;
                    }
                    else if (totalSelisih[2] > totalSelisih[0] && totalSelisih[2] > totalSelisih[1])
                    {
                        filename = "gol3_v2_10k.xml";
                        sum = 2;
                    }
                    //filename = "gol3_v2_10k.xml";
                   // sum = 2;
                     */

                    nnr.read(filename);
                    //Console.WriteLine("sum:" + sum);
                    Console.WriteLine(nnr.chromosom.Length);
                    for (int i = 0; i < dsl.Count; i++)
                    {
                        if (algoTest.Equals("GABP"))
                        {
                            int popCount = 0;
                            for (int j = 0; j < nnr.chromosom.Length; j++)
                            {
                                if (nnr.chromosom[j] == 0)
                                {
                                    dsl[i].RemoveBit(j - popCount);
                                    popCount++;
                                }
                            }
                            //Console.WriteLine("MASUK-GA");
                        }
                    }
                    loadNet = new NeuralNetwork();
                    nnr = new NNtoXMLReader(loadNet);
                    nnr.read(filename);
                    Console.WriteLine(filename);

                    ff = new FeedForward();
                    
                    ff.Initialise(loadNet, dsl);
                    for (int i = 0; i < dsl.Count; i++)
                    {
                        ff.Run(i);
                    }
                    t = new Translate(ff.GetActualClass());

                    Console.WriteLine(totalSelisih[0]);
                    Console.WriteLine(totalSelisih[1]);
                    Console.WriteLine(totalSelisih[2]);
                    Console.WriteLine(sum);
                    totalSelisih = new float[3];
                    
                    terjemahan.Text = t.Result(sum);
                    string imgsrc = terjemahan.Text.ToString();
                    terjemahImg.Source = new BitmapImage(new Uri(@"Images/Huruf/"+terjemahan.Text.ToString()+".jpg", UriKind.Relative));
                }
            }
            if (isTrainingFinished)
            {
                progressRing2.Visibility = Visibility.Visible;
                imageTarget2.Visibility = Visibility.Hidden;
                //Console.WriteLine(isTrainingFinished);

                //Console.WriteLine(idleWait);
                idleWait += deltaTime;
                if (idleWait >= 1.0)
                {
                    mode = "testing";
                    GridDataTestingON.Visibility = Visibility.Hidden;
                    GridDataTesting.Visibility = Visibility.Visible;
                    idleWait = 0;
                }
            }
        }

        void CalibrateMode(Leap.Frame frame, float deltaTime)
        {
            Pointable pointable = frame.Pointables.Frontmost;
            HandList hands = frame.Hands;
            Hand hand = hands[0];
            float waitTime = 0;
            float countdownStart = 3;
            if (GuideButton.Margin.Left <= ImageTarget3FarRight && GuideButton.Margin.Left >= ImageTarget3FarLeft
                     && GuideButton.Margin.Top <= ImageTarget3FarBottom && GuideButton.Margin.Top >= ImageTarget3FarTop && mode.Equals("calibrating"))
            {
                idleTime += deltaTime;
                onCalibrating = true;
                if (BackToTestingImg2.Width >= 100 || BackToTestingImg2.Height >= 100)
                {
                    BackToTestingImg2.Width -= 5;
                    BackToTestingImg2.Height -= 5;
                }
            }
            else if (GuideButton.Margin.Left <= BackToTestingButton2FarRight && GuideButton.Margin.Left >= BackToTestingButton2FarLeft
                      && GuideButton.Margin.Top <= BackToTestingButton2FarBottom && GuideButton.Margin.Top >= BackToTestingButton2FarTop && mode.Equals("calibrating"))
            {
                backToTesting = true;
                idleTime += deltaTime;

                BackToTestingImg2.Width += 4;
                BackToTestingImg2.Height += 4;

            }
            //if ((hand.Fingers[2].TipPosition.z - hand.PalmPosition.z) <= -45.0f && onCalibrating && mode.Equals("calibrating"))
            //{
            //    munculGambarKalibrasi = true;
            //    idleTime += deltaTime;
            //    //MessageBox.Show("wa");
            //}
            else
            {
                idleShape = 0;
                idleTime = 0;
                onCalibrating = false;
                backToTesting = false;
                munculGambarKalibrasi = false;
                kalibrasiJariImg.Visibility = Visibility.Hidden;
                labelCursor3.Content = "Arahkan pointer ke titik merah";
                ImageTarget3.Source = new BitmapImage(new Uri(@"/Images/target-red.png", UriKind.Relative));
                if (BackToTestingImg2.Width >= 100 || BackToTestingImg2.Height >= 100)
                {
                    BackToTestingImg2.Width -= 5;
                    BackToTestingImg2.Height -= 5;
                }
            }
            if (backToTesting && idleTime > 1.0)
            {
                GridDataTesting.Visibility = Visibility.Visible;
                ImageTarget3.Visibility = Visibility.Hidden;
                progressRing3.Visibility = Visibility.Hidden;
                GridCalibrating.Visibility = Visibility.Hidden;
                mode = "testing";
            }
            if (onCalibrating && idleTime >=0.5f)
            {

                kalibrasiJariImg.Visibility = Visibility.Visible;
                if ((hand.Fingers[2].TipPosition.z - hand.PalmPosition.z) <= -80.0f)
                {
                    idleShape += deltaTime;
                    munculGambarKalibrasi = true;
                }
                else
                {
                    labelCursor3.Content = "Rapatkan jari anda seperti gambar di bawah ini";
                    munculGambarKalibrasi = false;
                }
            }
            if (onCalibrating && idleShape >= 1.0 && (countdownStart - idleTime) >= 0 && munculGambarKalibrasi)
            {
                labelCursor3.Content = "kalibrasi dilakukan dalam " + (countdownStart - idleShape).ToString("0") + "...";
            }
            if ((countdownStart - idleShape) <= 2.0 && onCalibrating && munculGambarKalibrasi)
            {
                ImageTarget3.Source = new BitmapImage(new Uri(@"/Images/target-yellow.png", UriKind.Relative));
            }
            if ((countdownStart - idleShape) <= 1.0 && onCalibrating && munculGambarKalibrasi)
            {
                ImageTarget3.Source = new BitmapImage(new Uri(@"/Images/target-green.png", UriKind.Relative));
            }

            if ((countdownStart - idleShape) <= 0 && !isCalibratingFinished && onCalibrating && munculGambarKalibrasi)
            {
                labelCursor3.Content = "tunggu beberapa saat...";
                isCalibratingOnGoing = true;
                kalibrasiJariImg.Visibility = Visibility.Hidden;
                CalibrationButton.Source = new BitmapImage(new Uri(@"/Images/calibrateButtonAfter.png", UriKind.Relative));
            }
            if (isCalibratingOnGoing)
            {
                //Console.WriteLine("menyimpan isyarat...");
                pointable = frame.Pointables.Frontmost;
                hands = frame.Hands;
                hand = hands[0];

                kalibrasiLebarTangan = Math.Abs(hand.Fingers[0].TipPosition.x - hand.PalmPosition.x) + Math.Abs(hand.Fingers[4].TipPosition.x - hand.PalmPosition.x);
                kalibrasiPanjangTangan = Math.Abs(hand.Fingers[2].TipPosition.z - hand.PalmPosition.z);
               
                //MessageBox.Show("Lebar Tangan: " + kalibrasiLebarTangan.ToString() + " mm");
                //MessageBox.Show("Panjang Tangan: " + kalibrasiPanjangTangan.ToString() + " mm");

                isCalibratingOnGoing = false;
                isCalibratingFinished = true;
            }

            if (isCalibratingFinished)
            {
                progressRing3.Visibility = Visibility.Visible;
                ImageTarget3.Visibility = Visibility.Hidden;
                isCalibratingFinished = false;
                //Console.WriteLine(isTrainingFinished);
                //tabItemFitur.Header = "Ekstraksi Fitur " + "(" + DateTime.Now.ToString("h:mm:ss tt") + ")";
                //Console.WriteLine(idleWait);

                PanjangTanganTextBlock.Text = "Panjang: " + kalibrasiPanjangTangan.ToString() + " mm";
                LebarTanganTextBlock.Text = "Lebar: " + kalibrasiLebarTangan.ToString() + " mm";
                idleWait += deltaTime;
                if (idleWait >= 2.0)
                {
                    GridCalibrating.Visibility = Visibility.Hidden;
                    mode = "testing";
                    //GridTrainingSave.Visibility = Visibility.Visible;
                    idleWait = 0;
                }
            }
        }

        void OnTrainingMode(Leap.Frame frame, float deltaTime)
        {
            BackToTrainingButton.Opacity = 1;
            float countdownStart = 4;
           
            if (GuideButton.Margin.Left <= ImageTargetFarRight && GuideButton.Margin.Left >= ImageTargetFarLeft
                      && GuideButton.Margin.Top <= ImageTargetFarBottom && GuideButton.Margin.Top >= ImageTargetFarTop && mode.Equals("onTraining"))
            {
                idleTime += deltaTime;
                if(frame.Hands[0].PalmPosition.y >=400.0f && !isTrainingON)
                    labelCursor.Content = "Dekatkan telapak anda ke Leap Motion";
                else if (frame.Hands[0].PalmPosition.y < 100.0f && !isTrainingON)
                    labelCursor.Content = "Jauhkan telapak anda dari Leap Motion";
                else
                {
                    isTrainingON = true;
                }
            }
            else if (GuideButton.Margin.Left <= BackToTrainingButtonFarRight && GuideButton.Margin.Left >= BackToTrainingButtonFarLeft
                      && GuideButton.Margin.Top <= BackToTrainingButtonFarBottom && GuideButton.Margin.Top >= BackToTrainingButtonFarTop && mode.Equals("onTraining"))
            {
                backToTraining = true;
                idleTime += deltaTime;

                BackToTrainingButtonImg.Width += 4;
                BackToTrainingButtonImg.Height += 4;
  
            }
            else 
            {
                isTrainingON = false;
                isTrainingOnGoing = false;
                backToTraining = false;
                labelCursor.Content = "gerakkan cursor sampai berada di area bulat berwarna merah";
                imageTarget.Source = new BitmapImage(new Uri(@"/Images/target-red.png", UriKind.Relative));
                idleTime = 0;

                if (BackToTrainingButtonImg.Width >= 100 || BackToTrainingButtonImg.Height >= 100)
                {
                    BackToTrainingButtonImg.Width -= 5;
                    BackToTrainingButtonImg.Height -= 5;
                }
            }
            if (backToTraining && idleTime > 1.0)
            {
                GridDataTraining.Visibility = Visibility.Visible;
                GridTrainingON.Visibility = Visibility.Hidden;
                mode = "training";
            }
            if (isTrainingON && idleTime >= 1.0 && (countdownStart - idleTime) >= 0)
            {
                labelCursor.Content = "menyimpan isyarat dalam " + (countdownStart - idleTime).ToString("0") + "...";
            }
            if((countdownStart - idleTime) <= 2.0 && isTrainingON)
            {
                imageTarget.Source = new BitmapImage(new Uri(@"/Images/target-yellow.png", UriKind.Relative));
            }
            if((countdownStart - idleTime) <= 1.0 && isTrainingON)
            {
                imageTarget.Source = new BitmapImage(new Uri(@"/Images/target-green.png", UriKind.Relative));
            }

            if ((countdownStart - idleTime) <= 0 && !isTrainingFinished && isTrainingON)
            {   
                labelCursor.Content = "tunggu beberapa saat...";
                isTrainingOnGoing = true;
            }

            if (isTrainingOnGoing)
            {
                //Console.WriteLine("menyimpan isyarat...");
                Pointable pointable = frame.Pointables.Frontmost;
                HandList hands = frame.Hands;
                Hand hand = hands[0];

                /*
                float totalTipPosX = 0, totalTipPosY = 0, totalTipPosZ = 0;
                for (int i = 0; i < 5; i++)
                {
                    totalTipPosX += hand.Fingers[i].TipPosition.x;
                    totalTipPosY += hand.Fingers[i].TipPosition.y;
                    totalTipPosZ += hand.Fingers[i].TipPosition.z;
                }

                totalTipPosX /= 5;
                totalTipPosY /= 5;
                totalTipPosZ /= 5;
                avgTipPosX += totalTipPosX;
                avgTipPosY += totalTipPosY;
                avgTipPosZ += totalTipPosZ;
                */

                /*
                length += pointable.Length;
                width += pointable.Width;
                tipPositionX += avgTipPosX;
                tipPositionY += avgTipPosY;
                tipPositionZ += avgTipPosZ;
                palmPositionX += hand.PalmPosition.x;
                palmPositionY += hand.PalmPosition.y;
                palmPositionZ += hand.PalmPosition.z;
                avgJariPalmX += (jari1PalmX + jari2PalmX + jari3PalmX + jari4PalmX + jari5PalmX) / 5.0f;
                avgJariPalmY += (jari1PalmY + jari2PalmY + jari3PalmY + jari4PalmY + jari5PalmY) / 5.0f;
                avgJariPalmZ += (jari1PalmZ + jari2PalmZ + jari3PalmZ + jari4PalmZ + jari5PalmZ) / 5.0f;
                */
                handSphereRadius += hand.SphereRadius;
                handPitch += Math.Abs(hand.Direction.Pitch * (float)(180.0 / Math.PI));
                handYaw += Math.Abs(hand.Direction.Yaw * (float)(180.0 / Math.PI));
                handRoll += Math.Abs(hand.Direction.Roll * (float)(180.0 / Math.PI));

                jari1PosX += hand.Fingers[0].TipPosition.x;
                jari2PosX += hand.Fingers[1].TipPosition.x;
                jari3PosX += hand.Fingers[2].TipPosition.x;
                jari4PosX += hand.Fingers[3].TipPosition.x;
                jari5PosX += hand.Fingers[4].TipPosition.x;
                jari1PosY += hand.Fingers[0].TipPosition.y;
                jari2PosY += hand.Fingers[1].TipPosition.y;
                jari3PosY += hand.Fingers[2].TipPosition.y;
                jari4PosY += hand.Fingers[3].TipPosition.y;
                jari5PosY += hand.Fingers[4].TipPosition.y;
                jari1PosZ += hand.Fingers[0].TipPosition.z;
                jari2PosZ += hand.Fingers[1].TipPosition.z;
                jari3PosZ += hand.Fingers[2].TipPosition.z;
                jari4PosZ += hand.Fingers[3].TipPosition.z;
                jari5PosZ += hand.Fingers[4].TipPosition.z;

                jari1PalmX += (hand.Fingers[0].TipPosition.x - hand.PalmPosition.x);
                jari2PalmX += (hand.Fingers[1].TipPosition.x - hand.PalmPosition.x);
                jari3PalmX += (hand.Fingers[2].TipPosition.x - hand.PalmPosition.x);
                jari4PalmX += (hand.Fingers[3].TipPosition.x - hand.PalmPosition.x);
                jari5PalmX += (hand.Fingers[4].TipPosition.x - hand.PalmPosition.x);
                jari1PalmY += (hand.Fingers[0].TipPosition.y - hand.PalmPosition.y);
                jari2PalmY += (hand.Fingers[1].TipPosition.y - hand.PalmPosition.y);
                jari3PalmY += (hand.Fingers[2].TipPosition.y - hand.PalmPosition.y);
                jari4PalmY += (hand.Fingers[3].TipPosition.y - hand.PalmPosition.y);
                jari5PalmY += (hand.Fingers[4].TipPosition.y - hand.PalmPosition.y);
                jari1PalmZ += (hand.Fingers[0].TipPosition.z - hand.PalmPosition.z);
                jari2PalmZ += (hand.Fingers[1].TipPosition.z - hand.PalmPosition.z);
                jari3PalmZ += (hand.Fingers[2].TipPosition.z - hand.PalmPosition.z);
                jari4PalmZ += (hand.Fingers[3].TipPosition.z - hand.PalmPosition.z);
                jari5PalmZ += (hand.Fingers[4].TipPosition.z - hand.PalmPosition.z);

                jari0ke1X += hand.Fingers[1].TipPosition.x - hand.Fingers[0].TipPosition.x;
                jari0ke2X += hand.Fingers[2].TipPosition.x - hand.Fingers[0].TipPosition.x;
                jari0ke3X += hand.Fingers[3].TipPosition.x - hand.Fingers[0].TipPosition.x;
                jari0ke4X += hand.Fingers[4].TipPosition.x - hand.Fingers[0].TipPosition.x;
                jari0ke1Y += hand.Fingers[1].TipPosition.y - hand.Fingers[0].TipPosition.y;
                jari0ke2Y += hand.Fingers[2].TipPosition.y - hand.Fingers[0].TipPosition.y;
                jari0ke3Y += hand.Fingers[3].TipPosition.y - hand.Fingers[0].TipPosition.y;
                jari0ke4Y += hand.Fingers[4].TipPosition.y - hand.Fingers[0].TipPosition.y;
                jari0ke1Z += hand.Fingers[1].TipPosition.z - hand.Fingers[0].TipPosition.z;
                jari0ke2Z += hand.Fingers[2].TipPosition.z - hand.Fingers[0].TipPosition.z;
                jari0ke3Z += hand.Fingers[3].TipPosition.z - hand.Fingers[0].TipPosition.z;
                jari0ke4Z += hand.Fingers[4].TipPosition.z - hand.Fingers[0].TipPosition.z;
                jari1ke2X += hand.Fingers[2].TipPosition.x - hand.Fingers[1].TipPosition.x;
                jari1ke2Y += hand.Fingers[2].TipPosition.y - hand.Fingers[1].TipPosition.y;
                jari1ke2Z += hand.Fingers[2].TipPosition.z - hand.Fingers[1].TipPosition.z;
                
                countOnTraining++;
                labelFrame.Content = countOnTraining.ToString() + " frame telah disimpan..";
                if (countOnTraining == 10)
                {
                    /*
                    length /= countOnTraining;
                    width /= countOnTraining;
                    tipPositionX /= countOnTraining;
                    tipPositionY /= countOnTraining;
                    tipPositionZ /= countOnTraining;
                    palmPositionX /= countOnTraining;
                    palmPositionY /= countOnTraining;
                    palmPositionZ /= countOnTraining;
                    avgTipPosX /= countOnTraining;
                    avgTipPosY /= countOnTraining;
                    avgTipPosZ /= countOnTraining;
                    avgJariPalmX /= countOnTraining;
                    avgJariPalmY /= countOnTraining;
                    avgJariPalmZ /= countOnTraining;
                    */

                    handSphereRadius /= countOnTraining;
                    handPitch /= countOnTraining;
                    handYaw /= countOnTraining;
                    handRoll /= countOnTraining;
                    jari1PalmX /= countOnTraining;
                    jari2PalmX /= countOnTraining;
                    jari3PalmX /= countOnTraining;
                    jari4PalmX /= countOnTraining;
                    jari5PalmX /= countOnTraining;
                    jari1PalmY /= countOnTraining;
                    jari2PalmY /= countOnTraining;
                    jari3PalmY /= countOnTraining;
                    jari4PalmY /= countOnTraining;
                    jari5PalmY /= countOnTraining;
                    jari1PalmZ /= countOnTraining;
                    jari2PalmZ /= countOnTraining;
                    jari3PalmZ /= countOnTraining;
                    jari4PalmZ /= countOnTraining;
                    jari5PalmZ /= countOnTraining;
                    jari1PosX /= countOnTraining;
                    jari2PosX /= countOnTraining;
                    jari3PosX /= countOnTraining;
                    jari4PosX /= countOnTraining;
                    jari5PosX /= countOnTraining;
                    jari1PosY /= countOnTraining;
                    jari2PosY /= countOnTraining;
                    jari3PosY /= countOnTraining;
                    jari4PosY /= countOnTraining;
                    jari5PosY /= countOnTraining;
                    jari1PosZ /= countOnTraining;
                    jari2PosZ /= countOnTraining;
                    jari3PosZ /= countOnTraining;
                    jari4PosZ /= countOnTraining;
                    jari5PosZ /= countOnTraining;
                    jari0ke1X /= countOnTraining;
                    jari0ke2X /= countOnTraining;
                    jari0ke3X /= countOnTraining;
                    jari0ke4X /= countOnTraining;
                    jari0ke1Y /= countOnTraining;
                    jari0ke2Y /= countOnTraining;
                    jari0ke3Y /= countOnTraining;
                    jari0ke4Y /= countOnTraining;
                    jari0ke1Z /= countOnTraining;
                    jari0ke2Z /= countOnTraining;
                    jari0ke3Z /= countOnTraining;
                    jari0ke4Z /= countOnTraining;
                    jari1ke2X /= countOnTraining;
                    jari1ke2Y /= countOnTraining;
                    jari1ke2Z /= countOnTraining;

                    isTrainingFinished = true;
                    isTrainingOnGoing = false;
                    features = new List<Feature>();
                    features.Add(new Feature() { Keterangan = "Radius telapak", Nilai = handSphereRadius, Satuan = "° derajat" });
                    features.Add(new Feature() { Keterangan = "Sudut sumbu X (Pitch)", Nilai = handPitch, Satuan = "° derajat" });
                    features.Add(new Feature() { Keterangan = "Sudut sumbu Y (Yaw)", Nilai = handYaw, Satuan = "° derajat" });
                    features.Add(new Feature() { Keterangan = "Sudut sumbu Z (Roll)", Nilai = handRoll, Satuan = "° derajat" });
                    features.Add(new Feature() { Keterangan = "Jari 1 ke telapak (Sumbu X)", Nilai = jari1PalmX, Satuan = "milimeter" });
                    features.Add(new Feature() { Keterangan = "Jari 2 ke telapak (Sumbu X)", Nilai = jari2PalmX, Satuan = "milimeter" });
                    features.Add(new Feature() { Keterangan = "Jari 3 ke telapak (Sumbu X)", Nilai = jari3PalmX, Satuan = "milimeter" });
                    features.Add(new Feature() { Keterangan = "Jari 4 ke telapak (Sumbu X)", Nilai = jari4PalmX, Satuan = "milimeter" });
                    features.Add(new Feature() { Keterangan = "Jari 5 ke telapak (Sumbu X)", Nilai = jari5PalmX, Satuan = "milimeter" });
                    features.Add(new Feature() { Keterangan = "Jari 1 ke telapak (Sumbu Y)", Nilai = jari1PalmY, Satuan = "milimeter" });
                    features.Add(new Feature() { Keterangan = "Jari 2 ke telapak (Sumbu Y)", Nilai = jari2PalmY, Satuan = "milimeter" });
                    features.Add(new Feature() { Keterangan = "Jari 3 ke telapak (Sumbu Y)", Nilai = jari3PalmY, Satuan = "milimeter" });
                    features.Add(new Feature() { Keterangan = "Jari 4 ke telapak (Sumbu Y)", Nilai = jari4PalmY, Satuan = "milimeter" });
                    features.Add(new Feature() { Keterangan = "Jari 5 ke telapak (Sumbu Y)", Nilai = jari5PalmY, Satuan = "milimeter" });
                    features.Add(new Feature() { Keterangan = "Jari 1 ke telapak (Sumbu Z)", Nilai = jari1PalmZ, Satuan = "milimeter" });
                    features.Add(new Feature() { Keterangan = "Jari 2 ke telapak (Sumbu Z)", Nilai = jari2PalmZ, Satuan = "milimeter" });
                    features.Add(new Feature() { Keterangan = "Jari 3 ke telapak (Sumbu Z)", Nilai = jari3PalmZ, Satuan = "milimeter" });
                    features.Add(new Feature() { Keterangan = "Jari 4 ke telapak (Sumbu Z)", Nilai = jari4PalmZ, Satuan = "milimeter" });
                    features.Add(new Feature() { Keterangan = "Jari 5 ke telapak (Sumbu Z)", Nilai = jari5PalmZ, Satuan = "milimeter" });
                    features.Add(new Feature() { Keterangan = "Jari 1 ke jari 2 (Sumbu X)", Nilai = jari0ke1X, Satuan = "milimeter" });
                    features.Add(new Feature() { Keterangan = "Jari 1 ke jari 3 (Sumbu X)", Nilai = jari0ke2X, Satuan = "milimeter" });
                    features.Add(new Feature() { Keterangan = "Jari 1 ke jari 4 (Sumbu X)", Nilai = jari0ke3X, Satuan = "milimeter" });
                    features.Add(new Feature() { Keterangan = "Jari 1 ke jari 5 (Sumbu X)", Nilai = jari0ke4X, Satuan = "milimeter" });
                    features.Add(new Feature() { Keterangan = "Jari 1 ke jari 2 (Sumbu Y)", Nilai = jari0ke1Y, Satuan = "milimeter" });
                    features.Add(new Feature() { Keterangan = "Jari 1 ke jari 3 (Sumbu Y)", Nilai = jari0ke2Y, Satuan = "milimeter" });
                    features.Add(new Feature() { Keterangan = "Jari 1 ke jari 4 (Sumbu Y)", Nilai = jari0ke3Y, Satuan = "milimeter" });
                    features.Add(new Feature() { Keterangan = "Jari 1 ke jari 5 (Sumbu Y)", Nilai = jari0ke4Y, Satuan = "milimeter" });
                    features.Add(new Feature() { Keterangan = "Jari 1 ke jari 2 (Sumbu Z)", Nilai = jari0ke1Z, Satuan = "milimeter" });
                    features.Add(new Feature() { Keterangan = "Jari 1 ke jari 3 (Sumbu Z)", Nilai = jari0ke2Z, Satuan = "milimeter" });
                    features.Add(new Feature() { Keterangan = "Jari 1 ke jari 4 (Sumbu Z)", Nilai = jari0ke3Z, Satuan = "milimeter" });
                    features.Add(new Feature() { Keterangan = "Jari 1 ke jari 5 (Sumbu Z)", Nilai = jari0ke4Z, Satuan = "milimeter" });
                    features.Add(new Feature() { Keterangan = "Jari 2 ke jari 3 (Sumbu X)", Nilai = jari1ke2X, Satuan = "milimeter" });
                    features.Add(new Feature() { Keterangan = "Jari 2 ke jari 3 (Sumbu Y)", Nilai = jari1ke2Y, Satuan = "milimeter" });
                    features.Add(new Feature() { Keterangan = "Jari 2 ke jari 3 (Sumbu Z)", Nilai = jari1ke2Z, Satuan = "milimeter" });
                    listViewFitur.ItemsSource = features;
                    listViewFitur.Columns[0].Width = 200;
                    Console.WriteLine(features.Count);
                    /*
                    fitur1_lebarJari.Text = width.ToString("0.000") + " mm";
                    fitur2_panjangJari.Text = length.ToString("0.000") + " mm";
                    fitur3_ujungJariX.Text = avgTipPosX.ToString("0.000") + " mm";
                    fitur4_ujungJariY.Text = avgTipPosY.ToString("0.000") + " mm";
                    fitur5_ujungJariZ.Text = avgTipPosZ.ToString("0.000") + " mm";
                    fitur6_telapakX.Text = palmPositionX.ToString("0.000") + " mm";
                    fitur7_telapakY.Text = palmPositionY.ToString("0.000") + " mm";
                    fitur8_telapakZ.Text = palmPositionZ.ToString("0.000") + " mm";
                    fitur9_radiusKepal.Text = handSphereRadius.ToString("0.000") + " derajat";
                    fitur10_pitch.Text = handPitch.ToString("0.000") + " derajat";
                    fitur11_roll.Text = handRoll.ToString("0.000") + " derajat";
                    fitur12_yaw.Text = handYaw.ToString("0.000") + " derajat";
                     * */
                }
            }
            if (isTrainingFinished)
            {
                progressRing.Visibility = Visibility.Visible;
                imageTarget.Visibility = Visibility.Hidden;
                //Console.WriteLine(isTrainingFinished);
                //tabItemFitur.Header = "Ekstraksi Fitur " + "(" + DateTime.Now.ToString("h:mm:ss tt") + ")";
                //Console.WriteLine(idleWait);
                idleWait += deltaTime;
                if (idleWait >= 1.0)
                {
                    mode = "training";
                    GridTrainingON.Visibility = Visibility.Hidden;
                    //GridTrainingSave.Visibility = Visibility.Visible;
                    idleWait = 0;
                }
            }
        }

        void TrainingMode(Leap.Frame frame, float deltaTime)
        {

            listViewFitur.ItemsSource = features;
            GridDataTraining.Visibility = Visibility.Visible;
           
            //Console.WriteLine("////////////////////////////");
            //Console.WriteLine(frame.Hands[0].Direction.Pitch * (float)(180.0 / Math.PI));
            //Console.WriteLine(frame.Hands[0].Direction.Yaw * (float)(180.0 / Math.PI));
            //Console.WriteLine(Math.Abs(frame.Hands[0].Direction.Pitch * (float)(180.0 / Math.PI)) + " " + Math.Abs(frame.Hands[0].Direction.Yaw * (float)(180.0 / Math.PI)) + " " + Math.Abs(frame.Hands[0].Direction.Roll * (float)(180.0 / Math.PI)));
            //Console.WriteLine(frame.Hands[0].PalmPosition.x);
            //Console.WriteLine(frame.Hands[0].PalmPosition.y);
            //Console.WriteLine(frame.Hands[0].Fingers[0].TipPosition.z - frame.Hands[0].PalmPosition.z);
            //Console.WriteLine(frame.Hands[0].Fingers[1].TipPosition.z - frame.Hands[0].PalmPosition.z);
            //Console.WriteLine(frame.Hands[0].Fingers[2].TipPosition.z - frame.Hands[0].PalmPosition.z);
            //Console.WriteLine(frame.Hands[0].Fingers[3].TipPosition.z - frame.Hands[0].PalmPosition.z);
            //Console.WriteLine(frame.Hands[0].Fingers[4].TipPosition.x - frame.Hands[0].PalmPosition.x);
            //Console.WriteLine(frame.Hands[0].Fingers[1].StabilizedTipPosition.x + " " + frame.Hands[0].Fingers[1].TipPosition.x);
            //Console.WriteLine(frame.Hands[0].Fingers[0].TipPosition.x);
            //Console.WriteLine(frame.Hands[0].Fingers[0].TipPosition.y);
            //Console.WriteLine(frame.Hands[0].Fingers[0].TipPosition.z);
            if (GuideButton.Margin.Left <= StartButtonFarRight && GuideButton.Margin.Left >= StartButtonFarLeft
                       && GuideButton.Margin.Top <= StartButtonFarBottom && GuideButton.Margin.Top >= StartButtonFarTop && mode.Equals("training"))
            {
                //StartButton.Opacity = 0.25;

                isTraining = true;
                idleTime += deltaTime;
                StartButtonImg.Width += 4;
                StartButtonImg.Height += 4;
                if (BackToHomeButtonImg.Width >= 100 || BackToHomeButtonImg.Height >= 100)
                {
                    BackToHomeButtonImg.Width -= 5;
                    BackToHomeButtonImg.Height -= 5;
                }
            }
            else if (GuideButton.Margin.Left <= BackToHomeButtonFarRight && GuideButton.Margin.Left >= BackToHomeButtonFarLeft
                       && GuideButton.Margin.Top <= BackToHomeButtonFarBottom && GuideButton.Margin.Top >= BackToHomeButtonFarTop && mode.Equals("training"))
            {
                //BackToHomeButton.Opacity = 0.25;
                BackToHomeButtonImg.Width += 4;
                BackToHomeButtonImg.Height += 4;
                if (StartButtonImg.Width >= 200 || StartButtonImg.Height >= 200)
                {
                    StartButtonImg.Width -= 5;
                    StartButtonImg.Height -= 5;
                }
                backToHome = true;
                idleTime += deltaTime;
            }
            else
            {
                idleTime = 0;
                StartButton.Opacity = 1;
                BackToHomeButton.Opacity = 1;
                isTraining = false;
                if (StartButtonImg.Width >= 200 || StartButtonImg.Height >= 200)
                {
                    StartButtonImg.Width -= 5;
                    StartButtonImg.Height -= 5;
                }
                if (BackToHomeButtonImg.Width >= 100 || BackToHomeButtonImg.Height >= 100)
                {
                    BackToHomeButtonImg.Width -= 5;
                    BackToHomeButtonImg.Height -= 5;
                }
            }
            if (backToHome && idleTime > 1.0)
            {
                GridDataTraining.Visibility = Visibility.Hidden;
                GridHome.Visibility = Visibility.Visible;
                mode = "home";
                countGoToDataTraining = 0;
            }

            if (isTraining && idleTime > 1.0)
            {
                GridDataTraining.Visibility = Visibility.Hidden;
                GridTrainingON.Visibility = Visibility.Visible;
                GridHome.Visibility = Visibility.Hidden;
                imageTarget.Visibility = Visibility.Visible;
                progressRing.Visibility = Visibility.Hidden;
                countOnTraining = 0;
                labelFrame.Content = countOnTraining.ToString() + " frame telah disimpan..";
                //FITUR
                length= 0;
                width= 0;
                tipPositionX= 0;
                tipPositionY= 0;
                tipPositionZ= 0;
                palmPositionX= 0;
                palmPositionY= 0;
                palmPositionZ= 0;
                handSphereRadius= 0;
                handPitch= 0;
                handYaw= 0;
                handRoll= 0;
                avgTipPosX = 0;
                avgTipPosY = 0;
                avgTipPosZ = 0;
                jari1PalmX= 0;
                jari2PalmX= 0;
                jari3PalmX= 0;
                jari4PalmX= 0;
                jari5PalmX= 0;
                jari1PalmY= 0;
                jari2PalmY= 0;
                jari3PalmY= 0;
                jari4PalmY= 0;
                jari5PalmY= 0;
                jari1PalmZ= 0;
                jari2PalmZ= 0;
                jari3PalmZ= 0;
                jari4PalmZ= 0;
                jari5PalmZ= 0;
                jari1PosX=0;
                jari2PosX=0;
                jari3PosX=0;
                jari4PosX=0;
                jari5PosX=0;
                jari1PosY=0;
                jari2PosY=0;
                jari3PosY=0;
                jari4PosY=0;
                jari5PosY=0;
                jari1PosZ=0;
                jari2PosZ=0;
                jari3PosZ=0;
                jari4PosZ=0;
                jari5PosZ=0;
                jari0ke1X = 0;
                jari0ke2X = 0;
                jari0ke3X = 0;
                jari0ke4X = 0;
                jari0ke1Y = 0;
                jari0ke2Y = 0;
                jari0ke3Y = 0;
                jari0ke4Y = 0;
                jari0ke1Z = 0;
                jari0ke2Z = 0;
                jari0ke3Z = 0;
                jari0ke4Z = 0;
                jari1ke2X = 0;
                jari1ke2Y = 0;
                jari1ke2Z = 0;

                avgJariPalmX = 0;
                avgJariPalmY = 0;
                avgJariPalmX = 0;

                isTrainingFinished = false;
                isTraining = false;
                mode = "onTraining";
                
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string className = textBox_kelas.Text;
            DirectoryInfo d = new DirectoryInfo("DataSet");
            FileInfo[] Files = d.GetFiles("*.txt");
            int count = 0;
            foreach (FileInfo file in Files)
            {
                //Console.WriteLine(file.Name.ToLower() + "->" + className.ToLower());
                string fileName = file.Name.ToLower().Split('.')[0];
                if(fileName.Equals(className.ToLower()))
                {
                    count++;
                }
            }
            //Console.WriteLine(count);
           
            File = new StreamWriter("DataSet\\" + className + "." + count + ".txt");

            //File.WriteLine(length);
            //File.WriteLine(width);
            //File.WriteLine(avgTipPosX);
            //File.WriteLine(avgTipPosY);
            //File.WriteLine(avgTipPosZ);
            //File.WriteLine(palmPositionX);
            //File.WriteLine(palmPositionY);
            //File.WriteLine(palmPositionZ);
            //File.WriteLine(avgJariPalmX);
            //File.WriteLine(avgJariPalmY);
            //File.WriteLine(avgJariPalmZ);
            File.WriteLine(handSphereRadius);
            File.WriteLine(handPitch);
            File.WriteLine(handYaw);
            File.WriteLine(handRoll);
            File.WriteLine(jari1PalmX);
            File.WriteLine(jari2PalmX);
            File.WriteLine(jari3PalmX);
            File.WriteLine(jari4PalmX);
            File.WriteLine(jari5PalmX);
            File.WriteLine(jari1PalmY);
            File.WriteLine(jari2PalmY);
            File.WriteLine(jari3PalmY);
            File.WriteLine(jari4PalmY);
            File.WriteLine(jari5PalmY);
            File.WriteLine(jari1PalmZ);
            File.WriteLine(jari2PalmZ);
            File.WriteLine(jari3PalmZ);
            File.WriteLine(jari4PalmZ);
            File.WriteLine(jari5PalmZ);
            File.WriteLine(jari0ke1X);
            File.WriteLine(jari0ke2X);
            File.WriteLine(jari0ke3X);
            File.WriteLine(jari0ke4X);
            File.WriteLine(jari0ke1Y);
            File.WriteLine(jari0ke2Y);
            File.WriteLine(jari0ke3Y);
            File.WriteLine(jari0ke4Y);
            File.WriteLine(jari0ke1Z);
            File.WriteLine(jari0ke2Z);
            File.WriteLine(jari0ke3Z);
            File.WriteLine(jari0ke4Z);
            File.WriteLine(jari1ke2X);
            File.WriteLine(jari1ke2Y);
            File.WriteLine(jari1ke2Z);

           
            //File.WriteLine(jari1PosX);
            //File.WriteLine(jari2PosX);
            //File.WriteLine(jari3PosX);
            //File.WriteLine(jari4PosX);
            //File.WriteLine(jari5PosX);
            //File.WriteLine(jari1PosY);
            //File.WriteLine(jari2PosY);
            //File.WriteLine(jari3PosY);
            //File.WriteLine(jari4PosY);
            //File.WriteLine(jari5PosY);
            //File.WriteLine(jari1PosZ);
            //File.WriteLine(jari2PosZ);
            //File.WriteLine(jari3PosZ);
            //File.WriteLine(jari4PosZ);
            //File.WriteLine(jari5PosZ);
            File.Close();
       
            MessageBox.Show("Fitur telah tersimpan di file => "+className+"."+count+".txt");
            //textBox_kelas.Clear();
        }

        private void btnOpenNN_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.DefaultExt = ".xml"; // Default file extension
            openFileDialog1.Filter = "XML File (.xml)|*.xml"; // Filter files by extension 

            // Show open file dialog box
            Nullable<bool> result = openFileDialog1.ShowDialog();

            // Process open file dialog box results 
            if (result == true)
            {
                // Open document 
                filename = openFileDialog1.SafeFileName;
            }

        }

        private void btnTrainData_Click(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            OpenFileDialog ofd = new OpenFileDialog();
            fbd.SelectedPath = ofd.InitialDirectory;
            DialogResult dr = fbd.ShowDialog();
            ListDataSet dsl = new ListDataSet();
            NeuralNetwork nn = new NeuralNetwork();

            float avgError = 0;
            if (dr == System.Windows.Forms.DialogResult.OK)
            {
                DirectoryInfo info = new DirectoryInfo(fbd.SelectedPath);
                FileReader fr = new FileReader();

                dsl = fr.ReadFile(info.Name, cc);
              

                using (StreamWriter outfile = new StreamWriter("dataset35fitur.csv"))
                {
                    outfile.Write("handSphereRadius" + ",");
                    outfile.Write("handPitch" + ",");
                    outfile.Write("handYaw" + ",");
                    outfile.Write("handRoll" + ",");
                    outfile.Write("jari1PalmX" + ",");
                    outfile.Write("jari2PalmX" + ",");
                    outfile.Write("jari3PalmX" + ",");
                    outfile.Write("jari4PalmX" + ",");
                    outfile.Write("jari5PalmX" + ",");
                    outfile.Write("jari1PalmY" + ",");
                    outfile.Write("jari2PalmY" + ",");
                    outfile.Write("jari3PalmY" + ",");
                    outfile.Write("jari4PalmY" + ",");
                    outfile.Write("jari5PalmY" + ",");
                    outfile.Write("jari1PalmZ" + ",");
                    outfile.Write("jari2PalmZ" + ",");
                    outfile.Write("jari3PalmZ" + ",");
                    outfile.Write("jari4PalmZ" + ",");
                    outfile.Write("jari5PalmZ" + ",");
                    outfile.Write("jari0ke1X" + ","); 
                    outfile.Write("jari0ke2X" + ","); 
                    outfile.Write("jari0ke3X" + ","); 
                    outfile.Write("jari0ke4X" + ","); 
                    outfile.Write("jari0ke1Y" + ","); 
                    outfile.Write("jari0ke2Y" + ","); 
                    outfile.Write("jari0ke3Y" + ","); 
                    outfile.Write("jari0ke4Y" + ","); 
                    outfile.Write("jari0ke1Z" + ","); 
                    outfile.Write("jari0ke2Z" + ","); 
                    outfile.Write("jari0ke3Z" + ","); 
                    outfile.Write("jari0ke4Z" + ","); 
                    outfile.Write("jari1ke2X" + ","); 
                    outfile.Write("jari1ke2Y" + ","); 
                    outfile.Write("jari1ke2Z" + ","); 
                    outfile.Write(outfile.NewLine);
                    for (int x = 0; x < dsl.Count; x++)
                    {
                        string content = "";
                        for (int y = 0; y < dsl[x].AttributeCount; y++)
                        {
                            outfile.Write(dsl[x][y].ToString("0.0000") + ",");
                        }
                        outfile.Write(outfile.NewLine);
                    }
                }
                dsl.Normalized();
               
                if(alghorithm.Equals("BP"))
                {
                    nn.InitialiseNetwork(dsl[0].AttributeCount, dsl[0].AttributeCount / 2, cc.TargetCount);
                    nn.Seed = 0;
                    nn.InitialiseWeight();
                    BackPropagation bp = new BackPropagation();
                    bp.Initialise(nn, dsl, cc);
                    avgError= bp.Run(Int32.Parse(txtBoxEpoch.Text));
                    //Console.WriteLine(Int32.Parse(txtBoxEpoch.Text));
                }
                else if (alghorithm.Equals("GABP"))
                {
                    GeneticAlgorithm ga = new GeneticAlgorithm();
                    ga.Initialize(dsl, cc);
                    fitChrom = ga.Run();
                    for (int i = 0; i < dsl.Count; i++)
                    {
                        int popCount = 0;
                        for (int j = 0; j < fitChrom.Length; j++)
                        {
                            if (fitChrom[j] == 0)
                            {
                                dsl[i].RemoveBit(j - popCount);
                                popCount++;
                            }
                        }
                    }
                    nn.InitialiseNetwork(dsl[0].AttributeCount, dsl[0].AttributeCount / 2, cc.TargetCount);
                    nn.Seed = 0;
                    nn.InitialiseWeight();
                    BackPropagation bp = new BackPropagation();
                    bp.Initialise(nn, dsl, cc);
                    avgError = bp.Run(Int32.Parse(txtBoxEpoch.Text));
                }
            }

            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.FileName = "";
            saveFileDialog1.DefaultExt = ".xml";
            saveFileDialog1.Filter = "XML File (.xml)|*.xml";

            Nullable<bool> resultsave = saveFileDialog1.ShowDialog();

            if (resultsave == true)
            {
                NNtoXMLWriter nnw = new NNtoXMLWriter(nn, avgError);
                nnw.Write(saveFileDialog1.SafeFileName,alghorithm, fitChrom);
                
            }
            // Process save file dialog box results
            FeedForward ff = new FeedForward();
            loadNet = new NeuralNetwork();
            NNtoXMLReader nnr = new NNtoXMLReader(loadNet);

            nnr.read(saveFileDialog1.FileName);
            ff.Initialise(loadNet, dsl);
            
            int jumlahBenar = 0;
            for (int i = 0; i < dsl.Count; i++)
            {
                if (alghorithm.Equals("GABP"))
                {
                    int popCount = 0;
                    for (int j = 0; j < nnr.chromosom.Length; j++)
                    {
                        if (nnr.chromosom[j] == 0)
                        {
                            dsl[i].RemoveBit(j - popCount);
                            popCount++;
                        }
                    }
                }
                ff.Run(i);
                bool cekData = true;
                for (int j = 0; j < cc.TargetCount; j++)
                {
                    //Console.Write(ff.GetActualClass()[j] + "--> " + cc.GetTarget(dsl[i].ClassName)[j] + " ");
                    if (ff.GetActualClass()[j] != cc.GetTarget(dsl[i].ClassName)[j])
                    {
                        cekData = false;
                        break;
                    }

                }
                if (cekData)
                    jumlahBenar++;
                //Console.WriteLine();
            }
            MessageBox.Show("Dari total " + dsl.Count + " datatest, yang benar adalah " + jumlahBenar + " akurasi network " + (((float)jumlahBenar/(float)dsl.Count)*100.0f )  + "%");


        }

        private void radioBtnAlgo_Checked(object sender, RoutedEventArgs e)
        {
            if (radioBtnAlgo.IsChecked == true)
            {
                alghorithm = "BP";
            }
            else if (radioBtnAlgo2.IsChecked == true)
            {
                alghorithm = "GABP";
            }
        }

        private void btnAlgo_Click(object sender, RoutedEventArgs e)
        {
            if (algoTest.Equals("BP"))
            {
                algoTest = "BPGA";
             
            }
            else
            {
                algoTest = "BP";
            }
            btnAlgo.Content = algoTest;
        }

        private void listViewFitur_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            e.Row.Header = (e.Row.GetIndex() + 1).ToString(); 
        }
    }
}
