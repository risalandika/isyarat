﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Isyarat
{
    class Translate
    {
        private int[] actualClass;
        private string binary;
        private int dec = 0;
        private List<string> huruf = new List<string>();
        private int[] p;
        private int sum;

        public Translate()
        {

        }

        public Translate(int[] p)
        {
            actualClass = p;
            for (int i = 0; i < p.Length; i++)
            {
                binary+= Convert.ToString(p[i]);
            }
        }

        public Translate(int[] p, int sum)
        {
            // TODO: Complete member initialization
            this.p = p;
            this.sum = sum;
        }

        public void Initialize(int code)
        {
            huruf.Clear();
            if (code == 0)
            {
                
                huruf.Add("A");
                huruf.Add("E");
                huruf.Add("I");
                huruf.Add("J");
                huruf.Add("M");
                huruf.Add("N");
                huruf.Add("S");
                huruf.Add("T");
                huruf.Add("Y");
                huruf.Add("-");
                huruf.Add("-");
                huruf.Add("-");
                huruf.Add("-");
                huruf.Add("-");
                huruf.Add("-");
                huruf.Add("-");
                /*
                huruf.Add("A");
                huruf.Add("B");
                huruf.Add("C");
                huruf.Add("D");
                huruf.Add("E");
                huruf.Add("F");
                huruf.Add("G");
                huruf.Add("H");
                 */
            }
            else if (code == 1)
            {
                huruf.Add("C");
                huruf.Add("D");
                huruf.Add("G");
                huruf.Add("H");
                huruf.Add("L");
                huruf.Add("O");
                huruf.Add("P");
                huruf.Add("Q");
                huruf.Add("X");
                huruf.Add("Y");
                huruf.Add("-");
                huruf.Add("-");
                huruf.Add("-");
                huruf.Add("-");
                huruf.Add("-");
                huruf.Add("-");
                huruf.Add("-");
                /*
                huruf.Add("O");
                huruf.Add("P");
                huruf.Add("Q");
                huruf.Add("R");
                huruf.Add("S");
                huruf.Add("T");
                huruf.Add("U");
                huruf.Add("V");
                huruf.Add("W");
                huruf.Add("X");
                huruf.Add("Y");
                huruf.Add("Z");
                huruf.Add("-");
                huruf.Add("-");
                huruf.Add("-");
                huruf.Add("-");
                 * */
            }
            else if (code == 2)
            {
                huruf.Add("B");
                huruf.Add("F");
                huruf.Add("K");
                huruf.Add("R");
                huruf.Add("U");
                huruf.Add("V");
                huruf.Add("W");
                huruf.Add("Z");
            }
        }

        public String Result(int kode)
        {
            Initialize(kode);
            string hasil = huruf[Convert.ToInt32(binary,2)];
            return hasil;
        }
    }
}
